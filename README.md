<!--
SPDX-FileCopyrightText: 2024 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: CC0-1.0
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/fsfe-cd-mockup/00_README)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/fsfe-cd-mockldap/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/fsfe-cd-mockldap)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/fsfe-cd-mockldap)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/fsfe-cd-mockldap)

Status
------

**This project is unmaintained**. It was originally spun off of
django-auth-ldap, which no longer requires it. If you have a use for it, feel
free to copy the code for your own purposes (it's only for testing, after all).

Documentation may still be available at https://mockldap.readthedocs.io/.
