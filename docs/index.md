<!--
SPDX-FileCopyrightText: 2024 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: CC0-1.0
-->

# mockldap

## Status

**This project is unmaintained**. It was originally spun off of
django-auth-ldap, which no longer requires it. If you have a use for it,
feel free to copy the code for your own purposes (it\'s only for
testing, after all).

Documentation is now available at [docs.fsfe.org](https://docs.fsfe.org/en/repodocs/fsfe-cd-mockldap)

Old documentation may still be available at
<https://mockldap.readthedocs.io/>.

