<!--
SPDX-FileCopyrightText: 2024 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: CC0-1.0
-->

# LDAP Operations

Inside of an individual test, you will be performing some task that
involves LDAP operations and then verifying the outcome. In some cases,
you will need to prepare your mock
`~mockldap.LDAPObject`{.interpreted-text role="class"} to return
specific results for a given API call.

## LDAPObject

::: {.autoclass members=""}
mockldap.LDAPObject
:::

Every LDAP method on `~mockldap.LDAPObject`{.interpreted-text
role="class"} is actually an instance of
`~mockldap.recording.RecordedMethod`{.interpreted-text role="class"},
which allows you to set return values in advance for different sets of
arguments.

::: {.autoclass members=""}
mockldap.recording.RecordedMethod
:::

::: autoexception
mockldap.SeedRequired
:::
