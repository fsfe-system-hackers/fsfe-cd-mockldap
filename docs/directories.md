<!--
SPDX-FileCopyrightText: 2024 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: CC0-1.0
-->

# Mock Directories

The first step to using mockldap is to define some static LDAP content
and install it as a mock `~mockldap.LDAPObject`{.interpreted-text
role="class"}. For this, you will use
`mockldap.MockLdap`{.interpreted-text role="class"}. Only one instance
of this class should exist at a time;
`~unittest.TestCase.setUpClass`{.interpreted-text role="meth"} is a good
place to instantiate it.

`~mockldap.MockLdap`{.interpreted-text role="class"} can mock multiple
LDAP directories, identified by URI. You can provide directory content
for URIs individually and you can also provide default content for
connections to any unrecognized URI. If the code under test is only
expected to make one LDAP connection, the simplest option is just to
provide default content. If you need multiple directories, you can call
`~mockldap.MockLdap.set_directory`{.interpreted-text role="meth"} on
your `~mockldap.MockLdap`{.interpreted-text role="class"} instance.

LDAP content takes the form of a Python dictionary. Each key is a
distinguished name in string form; each value is a dictionary mapping
attributes to lists of values. In other words, `directory.items()`
should take the same form as results from
`~ldap.LDAPObject.search_s`{.interpreted-text role="meth"}.

    directory = {
        'uid=alice,ou=people,o=test': {
            'uid': ['alice'],
            'objectClass': ['person', 'organizationalPerson', 'inetOrgPerson', 'posixAccount'],
            'userPassword': ['password'],
            'uidNumber': ['1000'],
            'gidNumber': ['1000'],
            'givenName': ['Alice'],
            'sn': ['Adams']
        },
        'cn=active,ou=groups,o=test': {
            'cn': ['active'],
            'objectClass': ['groupOfNames'],
            'member': ['uid=alice,ou=people,o=test']
        },
    }

`~mockldap.MockLdap`{.interpreted-text role="class"} is stateful. The
overview shows a complete `example <example>`{.interpreted-text
role="ref"}, but following are the enumerated steps.

For some collection of tests:

> -   Instantiate `~mockldap.MockLdap`{.interpreted-text role="class"}.
>     Optionally pass in default directory contents.
> -   `Add content <mockldap.MockLdap.set_directory>`{.interpreted-text
>     role="meth"} for any additional directories. This is only
>     necessary if the code under test will connect to multiple LDAP
>     directories.

For each test:

> -   Just before an individual test, call
>     `~mockldap.MockLdap.start`{.interpreted-text role="meth"}. This
>     will instantiate your mock directories and patch
>     `ldap.initialize`{.interpreted-text role="func"}. You may need to
>     call this multiple times if `~ldap.initialize`{.interpreted-text
>     role="func"} is accessed by multiple names.
> -   Any time during your test, you can access an individual
>     `~mockldap.LDAPObject`{.interpreted-text role="class"} as
>     `mockldap[uri]`. This will let you seed return values for LDAP
>     operations and recover the record of which operations were
>     performed.
> -   After the test, call `~mockldap.MockLdap.stop`{.interpreted-text
>     role="meth"} or `~mockldap.MockLdap.stop_all`{.interpreted-text
>     role="meth"}.

::: warning
::: title
Warning
:::

The code under test must not keep an LDAP \"connection\" open across
individual test cases. If it does, it will be sharing a mock
`~mockldap.LDAPObject`{.interpreted-text role="class"} across tests, so
any state mutations will persist.
:::

## MockLdap

::: {.autoclass members=""}
mockldap.MockLdap
:::
